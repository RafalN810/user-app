## Design

### Header
Header contains logo and menu.
Menu is wrapped to a dropdown one if the screen size is smaller than or equal to 576px.
Currently displayed page is highlighted on the menu.
Dropdown menu is closed every time the user chooses an option or clicks outside of the menu.
Menu is hidden when users scrolls the page.

### Content
Content is vertically scrollable if necessary.
The smallest width allowed is 300px, below that value content is horizontally scrollable .

### Footer
Footer is displayed only if the screen size is bigger than 576px.
Footer is hidden when users scrolls the page.

## Functionality
User has the possibility to either submit or reset the form.

### Validation rules
The form is validated according to the following rules:
1. First name needs to be at least 2 characters long, but shouldn't exceed 20 characters.
2. First name allows typing in two parts separated by a single whitespace, like: "First name", however the single part cannot be shorter than 3 characters.
3. Last name needs to be at least 2 characters long, but shouldn't exceed 20 characters.
4. Last name allows typing in two parts separated by a single whitespace or by a single dash, like: "Last name" or "Last-name", however the single part cannot be shorter than 3 characters.
5. For both name fields no language specific characters are allowed.
6. Typed in email needs to follow the email pattern and is validated by a standard validator function. Allowed pattern is "name@domain.com".
7. Phone field accepts numbers with/or without the area code. The allowed pattern is +48123456789 or 123456789. Phone no should contain from 7 to 9 numbers.
8. Birthday date can be selected from a calendar input. Allowed values are from 1st January 1900 until the current day.
9. About input allows the user to type in text up to 255 characters long.
10. Avatar input accepts files of types: .png, .jpg, .jpeg. Type is checked without matching the case. The maximum allowed file size is 1MB.
11. Fields which are required to submit the form are: first name, last name, email, phone, birthday, but if the user will add the about description and avatar, then they need to be valid as well.

### Form behaviour
The form behaves in the following way:
1. After uploading a valid avatar file, the user is able to see its preview at the top of the form.
2. The error message is displayed right under invalid input, the input itself is marked with a red border. "Touching" the input also triggers the validation.
3. Submit button is disabled as long as the form is not filled correctly.
4. Reset button is enabled all the time.
5. After a successful submit, the user is redirected to the profile page, where all the input values are displayed.

## Technical description
Application is divided into 3 separate modules in order to easily extend or extract parts of it. The modules are named: `core`, `main` and `shared`.
The `core` module holds all the basic components of the app - header, footer, menu and the not-found component. In this module there is also the `SessionStoreService` defined.
The `main` module holds all the components and services, which are needed to display the user form and profile - `UserInformationFormComponent`, `UserProfileComponent` and also the `UserInformationWrapperService`, which acts as a layer in between the form component and the `FakeApiService` in order to handle the side actions.
The `shared` module holds the features, which can be commonly used across other possible features - directives, validators and also `FileUploadInputComponent`.

The data is prepared and handled by the `FakeApiService`, which should be replaced by the http client based service.

The SessionStoreService wraps the sessionStorage in order to provide all the methods needed to store and restore user data. Due to the asynchronous behaviour of the avatar file reading, the user data is restored with the help of `ReplaySubject` limited to 1 repeated value.
Every successful form submit triggers the `storeUserData` method, which then emits new values instantly (if no avatar is selected) or after a successful file load (if the avatar is selected). To prevent emitting of the new values, which are not different from the previously emitted ones, the `distinctUntilChanged` operator is used with the custom comparator method, which compares the object structure and value.

## Alternative solutions and possible improvements
The app could be improved by:
1. Better layout design, deeper analysys of the user needs and adding the missing ARIA related implementation.
2. Using a commonly used components libraries like PrimeNG or Angular Material to have the possibility to use already provided inputs, buttons, etc. and also their basic handling and styling.
3. Extending the validation of the input fields - for example regExp patterns supporting language specific characters.
4. Providing an implementation for the error handling - for example the popop dialog with the error desciribing message.