import { Component } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';

describe('AppComponent', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AppComponent, HeaderComponentMock, FooterComponentMock],
      imports: [RouterTestingModule],
    }).compileComponents();
  });

  it('should create the app', () => {
    // given
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;

    // when

    // then
    expect(app).toBeTruthy();
  });
});

@Component({
  selector: 'app-header',
  template: '',
})
export class HeaderComponentMock {}

@Component({
  selector: 'app-footer',
  template: '',
})
export class FooterComponentMock {}
