import { DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';
import { Translations } from 'ngx-translate-testing';

export const fillInput: Function = (
  debugElement: DebugElement,
  cssSelector: string,
  inputValue: string
): void => {
  const input: HTMLInputElement = debugElement.query(
    By.css(cssSelector)
  ).nativeElement;

  if (inputValue) {
    input.value = inputValue;
    input.dispatchEvent(new Event('input'));
  }

  input.dispatchEvent(new Event('blur'));
};

export const getInputValue: Function = (
  debugElement: DebugElement,
  cssSelector: string
): string => {
  return debugElement.query(By.css(cssSelector)).nativeElement.value;
};

declare var require: any;
export const plainTranslations: any = require('src/assets/i18n/en.json');
export const translations: Translations = {
  en: plainTranslations,
};
