import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { MenuItem } from 'src/app/api/model/menu-item';
import { UserData } from 'src/app/api/model/user-data';

@Injectable()
export class FakeApiServiceMock {
  constructor() {}

  public registerUser(userData: UserData): Observable<UserData> {
    return of(userData);
  }

  public getMenuItems(): Observable<MenuItem[]> {
    return of(menuItems);
  }
}

export const menuItems: MenuItem[] = [
  { orderingNo: 1, label: 'form', navUrl: 'user/form' },
  { orderingNo: 2, label: 'profile', navUrl: 'user/profile' },
  { orderingNo: 4, label: 'dummy2', navUrl: 'dummy2' },
  { orderingNo: 3, label: 'dummy1', navUrl: 'dummy1' },
  { orderingNo: 5, label: 'superLongDummy', navUrl: 'dummy3' },
];
