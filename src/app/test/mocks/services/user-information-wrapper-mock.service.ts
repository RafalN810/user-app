import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { UserData } from 'src/app/api/model/user-data';

@Injectable()
export class UserInformationWrapperServiceMock {
  constructor() {}

  public registerUser(userData: UserData): Observable<UserData> {
    return of(userData);
  }
}
