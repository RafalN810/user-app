import { Injectable } from '@angular/core';
import { Observable, ReplaySubject } from 'rxjs';
import { StoreUserData } from 'src/app/api/model/store-user-data';
import { UserData } from 'src/app/api/model/user-data';
import { SessionStoreService } from 'src/app/core/services/session-store.service';

@Injectable()
export class SessionStoreServiceMock extends SessionStoreService {
  private readonly userDataSubject: ReplaySubject<StoreUserData> =
    new ReplaySubject(1);

  constructor() {
    super();
  }

  public override storeUserData(userData: UserData): void {}

  public override restoreUserData(): Observable<StoreUserData> {
    const userData: StoreUserData = this.getUserDataMock();
    this.userDataSubject.next(userData);

    return this.userDataSubject;
  }

  private getUserDataMock(): StoreUserData {
    return storeUserDataMock;
  }
}

export const storeUserDataMock: StoreUserData = {
  firstName: 'firstName',
  lastName: 'lastName',
  email: 'email@email.com',
  phone: '+48123456789',
  birthday: new Date(2000, 1, 1, 12, 0, 0),
  about: 'about',
};
