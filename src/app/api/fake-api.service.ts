import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { MenuItem } from './model/menu-item';
import { UserData } from './model/user-data';

/**
 * Fake API service class. Should be replaced by httpClient based service.
 */
@Injectable({
  providedIn: 'root',
})
export class FakeApiService {
  constructor() {}

  /**
   * Registers user.
   * @param userData the data to register with
   * @returns the observable of an output of the registration
   */
  public registerUser(userData: UserData): Observable<UserData> {
    return of(userData);
  }

  /**
   * Gets the menu items.
   * @returns the observable of an array with menu items
   */
  public getMenuItems(): Observable<MenuItem[]> {
    const menuItems: MenuItem[] = [
      { orderingNo: 1, label: 'form', navUrl: 'user/form' },
      { orderingNo: 2, label: 'profile', navUrl: 'user/profile' },
      { orderingNo: 4, label: 'dummy2', navUrl: 'dummy2' },
      { orderingNo: 3, label: 'dummy1', navUrl: 'dummy1' },
      { orderingNo: 5, label: 'superLongDummy', navUrl: 'dummy3' },
    ];
    return of(menuItems);
  }
}
