export interface StoreUserData {
  firstName: string;
  lastName: string;
  email: string;
  phone: string;
  birthday: Date;
  about: string;
  avatar?: string | null;
}
