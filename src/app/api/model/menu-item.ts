export interface MenuItem {
  orderingNo: number;
  label: string;
  navUrl: string;
}
