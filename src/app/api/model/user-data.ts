export interface UserData {
  firstName: string;
  lastName: string;
  email: string;
  phone: string;
  birthday: Date;
  about: string;
  avatar: File;
}
