import { Injectable } from '@angular/core';
import { distinctUntilChanged, Observable, ReplaySubject } from 'rxjs';
import { UserData } from '../../api/model/user-data';
import { StoreUserData } from '../../api/model/store-user-data';

/**
 * Service responsible for storing data in the sessionStorage.
 */
@Injectable({
  providedIn: 'root',
})
export class SessionStoreService {
  private readonly userData$: ReplaySubject<StoreUserData> = new ReplaySubject(
    1
  );

  constructor() {}

  /**
   * Stores the user data in the session storage.
   * @param userData the data to be stored
   */
  public storeUserData(userData: UserData): void {
    const storeUserData: StoreUserData = this.mapToStore(userData);

    if (userData.avatar instanceof File) {
      // file reading is asynchronous
      const reader: FileReader = new FileReader();
      reader.onload = (_) => {
        window.sessionStorage.setItem(
          'userData',
          JSON.stringify(storeUserData)
        );
        window.sessionStorage.setItem('userAvatar', reader.result as string);
        this.userData$.next({
          ...storeUserData,
          avatar: reader.result as string,
        });
      };
      reader.readAsDataURL(userData.avatar);
    } else {
      window.sessionStorage.setItem('userData', JSON.stringify(storeUserData));
      window.sessionStorage.removeItem('userAvatar');
      this.userData$.next({
        ...storeUserData,
        avatar: null,
      });
    }
  }

  /**
   * Restores the user data from the sesion storage.
   * @returns the observable of a user data
   */
  public restoreUserData(): Observable<StoreUserData> {
    const userData: StoreUserData = this.getUserData();

    if (userData) {
      this.userData$.next(userData);
    }

    return this.userData$.pipe(
      distinctUntilChanged((prev, curr) => !this.areObjectsSame(prev, curr))
    );
  }

  private getUserData(): StoreUserData {
    const storedUserData: { [key: string]: unknown } = JSON.parse(
      window.sessionStorage.getItem('userData') as string
    );

    if (!storedUserData) {
      // storage empty
      return null as unknown as StoreUserData;
    }

    const storedAvatar: string = window.sessionStorage.getItem(
      'userAvatar'
    ) as string;

    return this.mapFromStore(storedUserData, storedAvatar);
  }

  private areObjectsSame(prev: StoreUserData, curr: StoreUserData): boolean {
    if (!prev && !curr) {
      // both falsy -> no change
      return false;
    }

    if ((!prev && curr) || (prev && !curr)) {
      // one falsy, one not -> change
      return true;
    }

    const prevKeys: string[] = Object.keys(prev);
    const currKeys: string[] = Object.keys(curr);
    if (prevKeys.length !== currKeys.length) {
      // different object structure
      return true;
    }

    // getting rid of any issues related with object comparison
    const normalizedPrev: { [key: string]: string } = JSON.parse(
      JSON.stringify(prev)
    );
    const normalizedCurr: { [key: string]: string } = JSON.parse(
      JSON.stringify(curr)
    );

    // compare
    for (let i = 0; i < prevKeys.length; i++) {
      // double check object structure
      if (!prevKeys.includes(currKeys[i])) {
        // different structure
        return true;
      }
      const currentKey: string = prevKeys[i];
      if (normalizedPrev[currentKey] !== normalizedCurr[currentKey]) {
        // different key value
        return true;
      }
    }
    return false;
  }

  private mapToStore(userData: UserData): StoreUserData {
    return {
      firstName: userData.firstName,
      lastName: userData.lastName,
      email: userData.email,
      phone: userData.phone,
      birthday: userData.birthday,
      about: userData.about,
    };
  }

  private mapFromStore(
    storedUserData: {
      [key: string]: unknown;
    },
    storedAvatar: string
  ): StoreUserData {
    return {
      firstName: storedUserData['firstName'] as string,
      lastName: storedUserData['lastName'] as string,
      email: storedUserData['email'] as string,
      phone: storedUserData['phone'] as string,
      birthday: new Date(storedUserData['birthday'] as string),
      about: storedUserData['about'] as string,
      avatar: storedAvatar as string,
    };
  }
}
