import { TestBed } from '@angular/core/testing';
import { StoreUserData } from 'src/app/api/model/store-user-data';
import { UserData } from 'src/app/api/model/user-data';
import { SessionStoreService } from './session-store.service';

describe('StoreService', () => {
  let service: SessionStoreService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SessionStoreService);
    window.sessionStorage.clear();
  });

  it('should create the service', () => {
    // given

    // when

    // then
    expect(service).toBeTruthy();
  });

  it('should store user data', (done) => {
    // given
    const mapToStoreSpy: jasmine.Spy = spyOn<any>(
      service,
      'mapToStore'
    ).and.callThrough();

    // when
    service.storeUserData(userDataMock);

    // then
    service.restoreUserData().subscribe((_) => {
      expect(mapToStoreSpy).toHaveBeenCalledTimes(1);
      expect(mapToStoreSpy).toHaveBeenCalledWith(userDataMock);
      expect(window.sessionStorage.getItem('userData')).toEqual(
        JSON.stringify(storeUserDataMock)
      );
      expect(window.sessionStorage.getItem('userAvatar')).not.toBe(null);

      done();
    });
  });

  it('should restore user data', (done) => {
    // given
    window.sessionStorage.setItem(
      'userData',
      JSON.stringify(storeUserDataMock)
    );
    window.sessionStorage.setItem('userAvatar', 'data:');

    const getUserDataSpy: jasmine.Spy = spyOn<any>(
      service,
      'getUserData'
    ).and.callThrough();

    const mapFromStoreSpy: jasmine.Spy = spyOn<any>(
      service,
      'mapFromStore'
    ).and.callThrough();

    // when-then
    service.restoreUserData().subscribe((storeUserData) => {
      expect(getUserDataSpy).toHaveBeenCalledTimes(1);
      expect(mapFromStoreSpy).toHaveBeenCalledTimes(1);
      expect(mapFromStoreSpy).toHaveBeenCalledWith(
        JSON.parse(JSON.stringify(storeUserDataMock)),
        storeUserDataMockWithAvatar.avatar
      );
      expect(storeUserData).toEqual(storeUserDataMockWithAvatar);

      done();
    });
  });

  it('should return true when comparing objects - one falsy', () => {
    // given

    // when
    const result: boolean = (service as any).areObjectsSame(
      null,
      storeUserDataMock
    );

    // when-then
    expect(result).toBeTrue();
  });

  it('should return true when comparing objects - different object keys amount', () => {
    // given

    // when
    const result: boolean = (service as any).areObjectsSame(
      storeUserDataMock,
      storeUserDataMockWithAvatar
    );

    // when-then
    expect(result).toBeTrue();
  });

  it('should return true when comparing objects - different structure', () => {
    // given

    // when
    const result: boolean = (service as any).areObjectsSame(
      { ...storeUserDataMock, someData: '' },
      storeUserDataMockWithAvatar
    );

    // when-then
    expect(result).toBeTrue();
  });

  it('should return true when comparing objects - different key value', () => {
    // given
    const storeUserData: StoreUserData = { ...storeUserDataMock };
    storeUserData.lastName = 'differentName';

    // when
    const result: boolean = (service as any).areObjectsSame(
      storeUserData,
      storeUserDataMock
    );

    // when-then
    expect(result).toBeTrue();
  });

  it('should return false when comparing objects - both falsy ', () => {
    // given

    // when
    const result: boolean = (service as any).areObjectsSame(null, undefined);

    // when-then
    expect(result).toBeFalse();
  });

  it('should return true when comparing objects - both same', () => {
    // given
    const storeUserData: StoreUserData = { ...storeUserDataMock };

    // when
    const result: boolean = (service as any).areObjectsSame(
      storeUserData,
      storeUserDataMock
    );

    // when-then
    expect(result).toBeFalse();
  });
});

const userDataMock: UserData = {
  firstName: 'firstName',
  lastName: 'lastName',
  email: 'email@email.com',
  phone: '+48123456789',
  birthday: new Date(2000, 1, 1, 1, 0, 0),
  about: 'about',
  avatar: new File([], 'fileName'),
};

const storeUserDataMock: StoreUserData = {
  firstName: 'firstName',
  lastName: 'lastName',
  email: 'email@email.com',
  phone: '+48123456789',
  birthday: new Date(2000, 1, 1, 1, 0, 0),
  about: 'about',
};

const storeUserDataMockWithAvatar: StoreUserData = {
  ...storeUserDataMock,
  avatar: 'data:',
};
