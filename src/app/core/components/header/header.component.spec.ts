import { Component, DebugElement } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { HeaderComponent } from './header.component';

describe('HeaderComponent', () => {
  let component: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;
  let debugElement: DebugElement;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [HeaderComponent, MenuComponentMock],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    debugElement = fixture.debugElement;
  });

  describe('class', () => {
    it('should create the component', () => {
      // given

      // when

      // then
      expect(component).toBeTruthy();
    });
  });

  describe('DOM', () => {
    it('should display the logo', () => {
      // given

      // when
      const img: HTMLImageElement | null = debugElement
        .query(By.css('#logo-container'))
        .nativeElement.querySelector('#logo');

      // then
      expect(img).toBeTruthy();
      expect(img?.src).toEqual(
        `${window.location.protocol}//${window.location.host}/assets/logo.png`
      );
    });

    it('should display the app-menu', () => {
      // given

      // when
      const appMenu: HTMLElement | null = debugElement.query(
        By.css('app-menu')
      ).nativeElement;

      // then
      expect(appMenu).toBeTruthy();
    });
  });
});

@Component({
  selector: 'app-menu',
  template: '',
})
export class MenuComponentMock {}
