import { Component } from '@angular/core';

/**
 * Component responsible for displaying the footer.
 */
@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
})
export class FooterComponent {
  public readonly footerText: string =
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. ' +
    'Cras dui ex, auctor ultrices mi vitae, dignissim bibendum lorem. ' +
    'Praesent ut nisl ipsum. Quisque efficitur luctus mauris, et varius lectus sagittis sit amet. ' +
    'Nam sit amet leo vel nibh rutrum placerat. Duis molestie egestas libero, ' +
    'vulputate placerat nibh tristique non. Aenean tincidunt magna ut ultricies venenatis. ' +
    'Donec finibus id nulla ac luctus. Suspendisse velit arcu, dignissim vitae libero id, ' +
    'dignissim ultricies orci. Mauris accumsan laoreet urna id blandit. Integer lacinia posuere feugiat.';
}
