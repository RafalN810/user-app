import { Location } from '@angular/common';
import {
  Component,
  DebugElement,
  EventEmitter,
  ViewChild,
} from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateTestingModule } from 'ngx-translate-testing';
import { MenuItem } from 'src/app/api/model/menu-item';
import { plainTranslations, translations } from 'src/app/test/common/helpers';
import { MenuItemComponent } from './menu-item.component';

describe('MenuItemComponent', () => {
  let hostComponent: TestHostComponent;
  let component: MenuItemComponent;
  let fixture: ComponentFixture<TestHostComponent>;
  let debugElement: DebugElement;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes([
          {
            path: 'user/form',
            component: TestHostComponent,
          },
        ]),
        TranslateTestingModule.withTranslations(translations),
      ],
      declarations: [TestHostComponent, MenuItemComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TestHostComponent);
    hostComponent = fixture.componentInstance;
    fixture.detectChanges();
    component = hostComponent.menuItemComponent;
    fixture.detectChanges();
    debugElement = fixture.debugElement;
  });

  describe('class', () => {
    it('should create the component', () => {
      // given

      // when

      // then
      expect(component).toBeTruthy();
    });

    it('should emit an event when onItemClick called', () => {
      // given
      const eventEmitterSpy: jasmine.Spy = spyOn<EventEmitter<void>>(
        component.itemClicked,
        'emit'
      ).and.callThrough();

      // when
      component.onItemClick();

      // then
      expect(eventEmitterSpy).toHaveBeenCalledTimes(1);
    });
  });

  describe('DOM', () => {
    it('should display item label', () => {
      // given

      // when
      const label: HTMLLinkElement = debugElement.query(
        By.css('.nav-link')
      ).nativeElement;

      // then
      const translation: string =
        plainTranslations['core']['menuItem'][component.item.label];
      expect(translation).toBeTruthy();
      expect(label.textContent).toEqual(translation);
    });

    it('should emit event when item clicked', () => {
      // given
      const eventEmitterSpy: jasmine.Spy = spyOn<EventEmitter<void>>(
        component.itemClicked,
        'emit'
      ).and.callThrough();

      // when
      const item: HTMLLinkElement = debugElement.query(
        By.css('.nav-link')
      ).nativeElement;

      item.click();

      // then
      expect(eventEmitterSpy).toHaveBeenCalledTimes(1);
    });

    it(
      'should navigate when item clicked',
      waitForAsync(() => {
        // given
        const location: Location = TestBed.inject(Location);

        // when
        const item: HTMLLinkElement = debugElement.query(
          By.css('.nav-link')
        ).nativeElement;

        item.click();

        // then
        fixture.whenStable().then(() => {
          expect(location.path()).toEqual(`/${component.item.navUrl}`);
        });
      })
    );
  });
});

@Component({
  selector: `host-component`,
  template: `<app-menu-item [item]="item"></app-menu-item>`,
})
class TestHostComponent {
  @ViewChild(MenuItemComponent)
  public menuItemComponent: MenuItemComponent;
  public item: MenuItem = {
    orderingNo: 1,
    label: 'form',
    navUrl: 'user/form',
  };
}
