import { Component, EventEmitter, Input, Output } from '@angular/core';
import { MenuItem } from 'src/app/api/model/menu-item';

/**
 * Component responsible for displaying the navigation menu item.
 */
@Component({
  selector: 'app-menu-item',
  templateUrl: './menu-item.component.html',
  styleUrls: ['./menu-item.component.scss'],
})
export class MenuItemComponent {
  @Input() public item: MenuItem;
  @Output() public itemClicked: EventEmitter<void> = new EventEmitter();

  /**
   * Emits an event whenever user clicks on an item.
   */
  public onItemClick(): void {
    this.itemClicked.emit();
  }
}
