import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subject, takeUntil } from 'rxjs';
import { FakeApiService } from 'src/app/api/fake-api.service';
import { MenuItem } from 'src/app/api/model/menu-item';

/**
 * Component responsible for displaying the navigation menu.
 */
@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
})
export class MenuComponent implements OnInit, OnDestroy {
  private readonly destroy$: Subject<void> = new Subject();
  public menuItems: MenuItem[];
  public showMenu: boolean = false;

  constructor(private readonly fakeApiService: FakeApiService) {}

  public ngOnInit(): void {
    this.initMenuItems();
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  /**
   * Switches showMenu flag.
   */
  public onToggle(): void {
    this.showMenu = !this.showMenu;
  }

  /**
   * Handles clickOutside action.
   */
  public onClickOutside(): void {
    this.showMenu = false;
  }

  /**
   * Handles itemClick action.
   */
  public onItemClick(): void {
    this.showMenu = false;
  }

  private initMenuItems() {
    this.fakeApiService
      .getMenuItems()
      .pipe(takeUntil(this.destroy$))
      .subscribe(
        (menuItems) => (this.menuItems = this.sortMenuItems(menuItems))
      );
  }

  private sortMenuItems(menuItems: MenuItem[]): MenuItem[] {
    return menuItems.sort((menuItemA, menuItemB) =>
      this.compareMenuItems(menuItemA, menuItemB)
    );
  }

  private compareMenuItems(menuItemA: MenuItem, menuItemB: MenuItem) {
    if (menuItemA.orderingNo < menuItemB.orderingNo) {
      return -1;
    }
    if (menuItemA.orderingNo > menuItemB.orderingNo) {
      return 1;
    }

    return 0;
  }
}
