import { Component, DebugElement, Input } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { FakeApiService } from 'src/app/api/fake-api.service';
import { MenuItem } from 'src/app/api/model/menu-item';
import {
  FakeApiServiceMock,
  menuItems,
} from 'src/app/test/mocks/services/fake-api-mock.service';
import { MenuComponent } from './menu.component';

describe('MenuComponent', () => {
  let component: MenuComponent;
  let fixture: ComponentFixture<MenuComponent>;
  let debugElement: DebugElement;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MenuComponent, MenuItemComponentMock],
      providers: [{ provide: FakeApiService, useClass: FakeApiServiceMock }],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    debugElement = fixture.debugElement;
  });

  describe('class', () => {
    it('should create the component', () => {
      // given

      // when

      // then
      expect(component).toBeTruthy();
    });

    it(
      'should get the menu items onInit',
      waitForAsync(() => {
        // given
        const getMenuItemsSpy: jasmine.Spy = spyOn<FakeApiService>(
          (component as any).fakeApiService,
          'getMenuItems'
        ).and.callThrough();

        // when
        component.ngOnInit();

        // then
        fixture.whenStable().then(() => {
          expect(getMenuItemsSpy).toHaveBeenCalledTimes(1);
        });
      })
    );

    it(
      'should get menu items when initMenuItems called',
      waitForAsync(() => {
        // given
        const getMenuItemsSpy: jasmine.Spy = spyOn<FakeApiService>(
          (component as any).fakeApiService,
          'getMenuItems'
        ).and.callThrough();

        const sortMenuItemsSpy: jasmine.Spy = spyOn<any>(
          component,
          'sortMenuItems'
        ).and.callThrough();

        // when
        (component as any).initMenuItems();

        // then
        fixture.whenStable().then(() => {
          expect(getMenuItemsSpy).toHaveBeenCalledTimes(1);
          expect(sortMenuItemsSpy).toHaveBeenCalledTimes(1);
          expect(sortMenuItemsSpy).toHaveBeenCalledWith(menuItems);
        });
      })
    );

    it('should sort menu items properly', () => {
      // given
      const menuItems: MenuItem[] = [
        { orderingNo: 2, label: 'form', navUrl: 'user/form' },
        { orderingNo: 1, label: 'profile', navUrl: 'user/profile' },
      ];

      // when
      const result: MenuItem[] = (component as any).sortMenuItems(menuItems);

      // then
      const menuOrderingNos: number[] = result.map((value) => value.orderingNo);
      expect(menuOrderingNos[0]).toEqual(1);
      expect(menuOrderingNos[1]).toEqual(2);
    });
  });

  describe('DOM', () => {
    it('should display 5 menu items', () => {
      // given

      // when
      const menuItems: DebugElement[] = debugElement.queryAll(
        By.css('app-menu-item')
      );

      // then
      expect(menuItems.length).toEqual(5);
    });
  });
});

@Component({
  selector: 'app-menu-item',
  template: '',
})
export class MenuItemComponentMock {
  @Input() public item: MenuItem;
}
