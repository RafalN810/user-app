import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { FooterComponent } from './components/footer/footer.component';
import { HeaderComponent } from './components/header/header.component';
import { MenuItemComponent } from './components/menu/menu-item/menu-item.component';
import { MenuComponent } from './components/menu/menu.component';
import { NotFoundComponent } from './components/not-found/not-found.component';

/**
 * Module responsible for defining all core functionalities.
 */
@NgModule({
  declarations: [
    FooterComponent,
    HeaderComponent,
    MenuComponent,
    MenuItemComponent,
    NotFoundComponent,
  ],
  imports: [CommonModule, RouterModule, SharedModule],
  exports: [
    FooterComponent,
    HeaderComponent,
    MenuComponent,
    MenuItemComponent,
    NotFoundComponent,
  ],
})
export class CoreModule {}
