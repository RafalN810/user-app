import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subject, takeUntil } from 'rxjs';
import { StoreUserData } from 'src/app/api/model/store-user-data';
import { SessionStoreService } from 'src/app/core/services/session-store.service';

/**
 * Component responsible for displaying user information.
 */
@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss'],
})
export class UserProfileComponent implements OnInit, OnDestroy {
  private readonly defaultImgSource: string = '/assets/avatar-placeholder.png';
  private readonly destroy$: Subject<void> = new Subject();
  public userData: StoreUserData;
  public imgSource: string = this.defaultImgSource;

  constructor(private readonly sessionStoreService: SessionStoreService) {}

  public ngOnInit(): void {
    this.restoreUserData();
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  private restoreUserData(): void {
    this.sessionStoreService
      .restoreUserData()
      .pipe(takeUntil(this.destroy$))
      .subscribe((userData) => {
        if (userData) {
          this.userData = userData;
          this.imgSource = userData.avatar ?? this.defaultImgSource;
        }
      });
  }
}
