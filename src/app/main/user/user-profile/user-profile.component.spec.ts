import { DatePipe } from '@angular/common';
import { DebugElement, LOCALE_ID } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { SessionStoreService } from 'src/app/core/services/session-store.service';
import {
  SessionStoreServiceMock,
  storeUserDataMock,
} from 'src/app/test/mocks/services/session-store-mock.service';
import { UserProfileComponent } from './user-profile.component';
import { TranslateTestingModule } from 'ngx-translate-testing';
import { translations } from 'src/app/test/common/helpers';

describe('UserProfileComponent', () => {
  let component: UserProfileComponent;
  let fixture: ComponentFixture<UserProfileComponent>;
  let debugElement: DebugElement;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [UserProfileComponent],
      imports: [TranslateTestingModule.withTranslations(translations)],
      providers: [
        { provide: SessionStoreService, useClass: SessionStoreServiceMock },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    debugElement = fixture.debugElement;
  });

  describe('class', () => {
    it('should create the component', () => {
      // given

      // when

      // then
      expect(component).toBeTruthy();
    });

    it('should restore user data', () => {
      // given

      // when

      // then
      expect(component.userData).toEqual(storeUserDataMock);
    });
  });

  describe('DOM', () => {
    it('should display user data', () => {
      // given

      // when
      const data: (string | null)[] = debugElement
        .queryAll(By.css('span'))
        .map(
          (element) => (element.nativeElement as HTMLSpanElement).textContent
        );

      const datePipe: DatePipe = new DatePipe(TestBed.inject(LOCALE_ID));

      // then
      expect(data.length).toEqual(Object.values(storeUserDataMock).length);
      Object.values(storeUserDataMock).forEach((value) => {
        expect(data).toContain(
          value instanceof Date ? datePipe.transform(value) : value
        );
      });
    });

    it('should display avatar placeholder', () => {
      // given

      // when
      const avatar: HTMLImageElement | null = debugElement
        .query(By.css('#avatar-preview'))
        .nativeElement.querySelector('#avatar-img');

      // then
      expect(avatar).toBeTruthy();
      expect(avatar?.src).toEqual(
        `${window.location.protocol}//${window.location.host}/assets/avatar-placeholder.png`
      );
    });
  });
});
