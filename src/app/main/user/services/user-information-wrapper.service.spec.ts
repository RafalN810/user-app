import { FakeApiService } from 'src/app/api/fake-api.service';
import { UserData } from 'src/app/api/model/user-data';
import { SessionStoreService } from 'src/app/core/services/session-store.service';
import { FakeApiServiceMock } from 'src/app/test/mocks/services/fake-api-mock.service';
import { SessionStoreServiceMock } from 'src/app/test/mocks/services/session-store-mock.service';
import { UserInformationWrapperService } from './user-information-wrapper.service';

describe('UserInformationWrapperService', () => {
  let service: UserInformationWrapperService;
  let fakeApiService: FakeApiService = new FakeApiServiceMock();
  let sessionStoreService: SessionStoreService = new SessionStoreServiceMock();

  beforeEach(() => {
    fakeApiService = new FakeApiServiceMock();
    sessionStoreService = new SessionStoreServiceMock();
    service = new UserInformationWrapperService(
      fakeApiService,
      sessionStoreService
    );
  });

  it('should create the service', () => {
    // given

    // when

    // then
    expect(service).toBeTruthy();
  });

  it('should register user', (done) => {
    // given

    // when-then
    service.registerUser(userDataMock).subscribe((userData) => {
      expect(userData).toEqual(userDataMock);

      done();
    });
  });

  it('should store data when registering user', (done) => {
    // given
    const storeUserSpy: jasmine.Spy = spyOn<SessionStoreService>(
      sessionStoreService,
      'storeUserData'
    ).and.callThrough();

    // when-then
    service.registerUser(userDataMock).subscribe((_) => {
      expect(storeUserSpy).toHaveBeenCalledTimes(1);
      expect(storeUserSpy).toHaveBeenCalledWith(userDataMock);

      done();
    });
  });
});

const userDataMock: UserData = {
  firstName: 'firstName',
  lastName: 'lastName',
  email: 'email@email.com',
  phone: '+48123456789',
  birthday: new Date(2000, 1, 1, 1, 0, 0),
  about: 'about',
  avatar: new File([], 'fileName'),
};
