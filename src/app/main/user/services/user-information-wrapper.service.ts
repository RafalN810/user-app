import { Injectable } from '@angular/core';
import { Observable, tap } from 'rxjs';
import { FakeApiService } from 'src/app/api/fake-api.service';
import { UserData } from 'src/app/api/model/user-data';
import { SessionStoreService } from 'src/app/core/services/session-store.service';

/**
 * Service responsible for wrapping the API service calls and trigger additional actions.
 */
@Injectable()
export class UserInformationWrapperService {
  constructor(
    private readonly fakeApiService: FakeApiService,
    private readonly sessionStoreService: SessionStoreService
  ) {}

  /**
   * Handles the registration process and stores the received data.
   * @param userData user data to be registered
   * @returns the observable of an output of the registration
   */
  public registerUser(userData: UserData): Observable<UserData> {
    return this.fakeApiService
      .registerUser(userData)
      .pipe(
        tap((userData) => this.sessionStoreService.storeUserData(userData))
      );
  }
}
