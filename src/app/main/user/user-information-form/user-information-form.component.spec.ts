import { DatePipe, Location } from '@angular/common';
import { Component, DebugElement, LOCALE_ID } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { RouterTestingModule } from '@angular/router/testing';
import { Subject } from 'rxjs';
import { UserData } from 'src/app/api/model/user-data';
import {
  fillInput,
  getInputValue,
  plainTranslations,
  translations,
} from 'src/app/test/common/helpers';
import { UserInformationWrapperServiceMock } from 'src/app/test/mocks/services/user-information-wrapper-mock.service';
import { UserInformationWrapperService } from '../services/user-information-wrapper.service';
import { UserInformationFormComponent } from './user-information-form.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { SharedModule } from 'src/app/shared/shared.module';
import { TranslateTestingModule } from 'ngx-translate-testing';

describe('UserInformationFormComponent', () => {
  let component: UserInformationFormComponent;
  let fixture: ComponentFixture<UserInformationFormComponent>;
  let debugElement: DebugElement;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        ReactiveFormsModule,
        RouterTestingModule.withRoutes([
          {
            path: 'user/profile',
            component: TestComponent,
          },
        ]),
        SharedModule,
        TranslateTestingModule.withTranslations(translations),
      ],
      declarations: [UserInformationFormComponent],
      providers: [
        {
          provide: UserInformationWrapperService,
          useClass: UserInformationWrapperServiceMock,
        },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserInformationFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    debugElement = fixture.debugElement;
  });

  describe('class', () => {
    it('should create the component', () => {
      // given

      // when

      // then
      expect(component).toBeTruthy();
    });

    it('should init the user information form group', () => {
      // given
      const expectedFormControlNames: string[] = [
        'firstName',
        'lastName',
        'email',
        'phone',
        'birthday',
        'about',
        'avatar',
      ];

      // when

      // then
      expect(component.userInformationFormGroup).toBeTruthy();
      expect(Object.keys(component.userInformationFormGroup.value)).toEqual(
        expectedFormControlNames
      );
    });

    it('should call wrapper service when form submitted', () => {
      // given
      const registerUserSpy: jasmine.Spy = spyOn<UserInformationWrapperService>(
        (component as any).userInformationWrapperService,
        'registerUser'
      ).and.callThrough();

      const mapToUserDataSpy: jasmine.Spy = spyOn<any>(
        component,
        'mapToUserData'
      ).and.callThrough();

      // when
      component.onFormSubmit();

      // then
      expect(registerUserSpy).toHaveBeenCalledTimes(1);
      expect(mapToUserDataSpy).toHaveBeenCalledTimes(1);
      expect(mapToUserDataSpy).toHaveBeenCalledWith(
        component.userInformationFormGroup.value
      );
    });

    it('should call handleFormSubmitted when form submitted', (done) => {
      // given
      const fakeResponse: Subject<UserData> = new Subject();
      spyOn<UserInformationWrapperService>(
        (component as any).userInformationWrapperService,
        'registerUser'
      ).and.returnValue(fakeResponse.asObservable());

      const handleFormSubmittedSpy: jasmine.Spy = spyOn<any>(
        component,
        'handleFormSubmitted'
      ).and.callThrough();

      // when
      component.onFormSubmit();

      // then
      fakeResponse.subscribe((_) => {
        expect(handleFormSubmittedSpy).toHaveBeenCalledTimes(1);
        expect(handleFormSubmittedSpy).toHaveBeenCalledWith(userDataMock);

        done();
      });

      fakeResponse.next(userDataMock);
    });

    it('should call handleAvatarChosen when avatar value changes', (done) => {
      // given
      const file: File = new File([], 'fileName');

      const handleAvatarChosenSpy: jasmine.Spy = spyOn<any>(
        component,
        'handleAvatarChosen'
      ).and.callThrough();

      // when

      // then
      component.avatarControl.valueChanges.subscribe((_) => {
        expect(handleAvatarChosenSpy).toHaveBeenCalledTimes(1);
        expect(handleAvatarChosenSpy).toHaveBeenCalledWith(file);

        done();
      });

      component.avatarControl.setValue(file);
    });

    it('should map form to user data properly', () => {
      // given
      const userDataToMap: { [key: string]: any } = {
        firstName: 'firstName',
        lastName: 'lastName',
        email: 'email',
        phone: 'phone',
        birthday: '2000-10-01',
        about: 'about',
        avatar: new File([], 'fileName'),
      };

      // when
      const result: UserData = (component as any).mapToUserData(userDataToMap);

      // then
      expect(result).toEqual(userDataToMap as UserData);
    });

    it('should call form reset on reset', () => {
      // given
      const resetFormSpy: jasmine.Spy = spyOn<any>(
        component.userInformationFormGroup,
        'reset'
      ).and.callThrough();

      // when
      component.onFormReset();

      // then
      expect(resetFormSpy).toHaveBeenCalledTimes(1);
    });
  });

  describe('DOM', () => {
    it(
      'should not display error message for first name input',
      waitForAsync(() => {
        // given

        // when
        fillInput(debugElement, '#first-name', 'FirstName');
        fixture.detectChanges();

        // then
        fixture.whenStable().then(() => {
          expect(debugElement.query(By.css('#first-name-error'))).toBeFalsy();
        });
      })
    );

    it(
      'should not display error message for first name input - two parts name',
      waitForAsync(() => {
        // given

        // when
        fillInput(debugElement, '#first-name', 'First Name');
        fixture.detectChanges();

        // then
        fixture.whenStable().then(() => {
          expect(debugElement.query(By.css('#first-name-error'))).toBeFalsy();
        });
      })
    );

    it(
      'should display error message for first name input when invalid - number',
      waitForAsync(() => {
        // given

        // when
        fillInput(debugElement, '#first-name', 'Name123');
        fixture.detectChanges();

        // then
        fixture.whenStable().then(() => {
          expectErrorMessage(
            debugElement,
            '#first-name-error',
            getFirstNameErrorMessage(component)
          );
        });
      })
    );

    it(
      'should display error message for first name input when invalid - invalid character',
      waitForAsync(() => {
        // given

        // when
        fillInput(debugElement, '#first-name', 'Name?');
        fixture.detectChanges();

        // then
        fixture.whenStable().then(() => {
          expectErrorMessage(
            debugElement,
            '#first-name-error',
            getFirstNameErrorMessage(component)
          );
        });
      })
    );

    it(
      'should display error message for first name input when invalid - short',
      waitForAsync(() => {
        // given

        // when
        fillInput(debugElement, '#first-name', 'X');
        fixture.detectChanges();

        // then
        fixture.whenStable().then(() => {
          expectErrorMessage(
            debugElement,
            '#first-name-error',
            getFirstNameErrorMessage(component)
          );
        });
      })
    );

    it(
      'should display error message for first name input when invalid - long',
      waitForAsync(() => {
        // given

        // when
        fillInput(debugElement, '#first-name', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ');
        fixture.detectChanges();

        // then
        fixture.whenStable().then(() => {
          expectErrorMessage(
            debugElement,
            '#first-name-error',
            getFirstNameErrorMessage(component)
          );
        });
      })
    );

    it(
      'should display error message for first name input - two whitespaces',
      waitForAsync(() => {
        // given

        // when
        fillInput(debugElement, '#first-name', 'First  Name');
        fixture.detectChanges();

        // then
        fixture.whenStable().then(() => {
          expectErrorMessage(
            debugElement,
            '#first-name-error',
            getFirstNameErrorMessage(component)
          );
        });
      })
    );

    it(
      'should display error message for first name input - one part too short',
      waitForAsync(() => {
        // given

        // when
        fillInput(debugElement, '#first-name', 'First Na');
        fixture.detectChanges();

        // then
        fixture.whenStable().then(() => {
          expectErrorMessage(
            debugElement,
            '#first-name-error',
            getFirstNameErrorMessage(component)
          );
        });
      })
    );

    it(
      'should display error message for first name input when touched',
      waitForAsync(() => {
        // given

        // when
        fillInput(debugElement, '#first-name', null);
        fixture.detectChanges();

        // then
        fixture.whenStable().then(() => {
          expectErrorMessage(
            debugElement,
            '#first-name-error',
            getFirstNameErrorMessage(component)
          );
        });
      })
    );

    it(
      'should not display error message for last name input',
      waitForAsync(() => {
        // given

        // when
        fillInput(debugElement, '#last-name', 'LastName');
        fixture.detectChanges();

        // then
        fixture.whenStable().then(() => {
          expect(debugElement.query(By.css('#last-name-error'))).toBeFalsy();
        });
      })
    );

    it(
      'should not display error message for last name input - two parts last name with whitespace',
      waitForAsync(() => {
        // given

        // when
        fillInput(debugElement, '#last-name', 'Last Name');
        fixture.detectChanges();

        // then
        fixture.whenStable().then(() => {
          expect(debugElement.query(By.css('#last-name-error'))).toBeFalsy();
        });
      })
    );

    it(
      'should not display error message for last name input - two parts last name with dash',
      waitForAsync(() => {
        // given

        // when
        fillInput(debugElement, '#last-name', 'First-Name');
        fixture.detectChanges();

        // then
        fixture.whenStable().then(() => {
          expect(debugElement.query(By.css('#last-name-error'))).toBeFalsy();
        });
      })
    );

    it(
      'should display error message for last name input when invalid - number',
      waitForAsync(() => {
        // given

        // when
        fillInput(debugElement, '#last-name', 'Name123');
        fixture.detectChanges();

        // then
        fixture.whenStable().then(() => {
          expectErrorMessage(
            debugElement,
            '#last-name-error',
            getLastNameErrorMessage(component)
          );
        });
      })
    );

    it(
      'should display error message for last name input when invalid - invalid character',
      waitForAsync(() => {
        // given

        // when
        fillInput(debugElement, '#last-name', 'Name?');
        fixture.detectChanges();

        // then
        fixture.whenStable().then(() => {
          expectErrorMessage(
            debugElement,
            '#last-name-error',
            getLastNameErrorMessage(component)
          );
        });
      })
    );

    it(
      'should display error message for last name input when invalid - short',
      waitForAsync(() => {
        // given

        // when
        fillInput(debugElement, '#last-name', 'X');
        fixture.detectChanges();

        // then
        fixture.whenStable().then(() => {
          expectErrorMessage(
            debugElement,
            '#last-name-error',
            getLastNameErrorMessage(component)
          );
        });
      })
    );

    it(
      'should display error message for last name input when invalid - long',
      waitForAsync(() => {
        // given

        // when
        fillInput(debugElement, '#last-name', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ');
        fixture.detectChanges();

        // then
        fixture.whenStable().then(() => {
          expectErrorMessage(
            debugElement,
            '#last-name-error',
            getLastNameErrorMessage(component)
          );
        });
      })
    );

    it(
      'should display error message for last name input when invalid - 2 whitespaces',
      waitForAsync(() => {
        // given

        // when
        fillInput(debugElement, '#last-name', 'Last  Name');
        fixture.detectChanges();

        // then
        fixture.whenStable().then(() => {
          expectErrorMessage(
            debugElement,
            '#last-name-error',
            getLastNameErrorMessage(component)
          );
        });
      })
    );

    it(
      'should display error message for last name input when invalid - 2 dashes',
      waitForAsync(() => {
        // given

        // when
        fillInput(debugElement, '#last-name', 'Last--Name');
        fixture.detectChanges();

        // then
        fixture.whenStable().then(() => {
          expectErrorMessage(
            debugElement,
            '#last-name-error',
            getLastNameErrorMessage(component)
          );
        });
      })
    );

    it(
      'should display error message for last name input when invalid - dash and whitespace',
      waitForAsync(() => {
        // given

        // when
        fillInput(debugElement, '#last-name', 'Last- Name');
        fixture.detectChanges();

        // then
        fixture.whenStable().then(() => {
          expectErrorMessage(
            debugElement,
            '#last-name-error',
            getLastNameErrorMessage(component)
          );
        });
      })
    );

    it(
      'should display error message for last name input when invalid - one part too short',
      waitForAsync(() => {
        // given

        // when
        fillInput(debugElement, '#last-name', 'Last Na');
        fixture.detectChanges();

        // then
        fixture.whenStable().then(() => {
          expectErrorMessage(
            debugElement,
            '#last-name-error',
            getLastNameErrorMessage(component)
          );
        });
      })
    );

    it(
      'should display error message for last name input when touched',
      waitForAsync(() => {
        // given

        // when
        fillInput(debugElement, '#last-name', null);
        fixture.detectChanges();

        // then
        fixture.whenStable().then(() => {
          expectErrorMessage(
            debugElement,
            '#last-name-error',
            getLastNameErrorMessage(component)
          );
        });
      })
    );

    it(
      'should not display error message for email input',
      waitForAsync(() => {
        // given

        // when
        fillInput(debugElement, '#email', 'email@email.com');
        fixture.detectChanges();

        // then
        fixture.whenStable().then(() => {
          expect(debugElement.query(By.css('#email-error'))).toBeFalsy();
        });
      })
    );

    it(
      'should display error message for email input when invalid',
      waitForAsync(() => {
        // given

        // when
        fillInput(debugElement, '#email', 'invalid@');
        fixture.detectChanges();

        // then
        fixture.whenStable().then(() => {
          expectErrorMessage(
            debugElement,
            '#email-error',
            plainTranslations['main']['userInformation']['emailError']
          );
        });
      })
    );

    it(
      'should display error message for email input when touched',
      waitForAsync(() => {
        // given

        // when
        fillInput(debugElement, '#email', null);
        fixture.detectChanges();

        // then
        fixture.whenStable().then(() => {
          expectErrorMessage(
            debugElement,
            '#email-error',
            plainTranslations['main']['userInformation']['emailError']
          );
        });
      })
    );

    it(
      'should not display error message for phone input',
      waitForAsync(() => {
        // given

        // when
        fillInput(debugElement, '#phone', '+48123456789');
        fixture.detectChanges();

        // then
        fixture.whenStable().then(() => {
          expect(debugElement.query(By.css('#phone-error'))).toBeFalsy();
        });
      })
    );

    it(
      'should display error message for phone input when invalid - invalid character',
      waitForAsync(() => {
        // given

        // when
        fillInput(debugElement, '#phone', '-48123456789');
        fixture.detectChanges();

        // then
        fixture.whenStable().then(() => {
          expectErrorMessage(
            debugElement,
            '#phone-error',
            plainTranslations['main']['userInformation']['phoneNoError']
          );
        });
      })
    );

    it(
      'should display error message for phone input when invalid - short',
      waitForAsync(() => {
        // given

        // when
        fillInput(debugElement, '#phone', '+481234');
        fixture.detectChanges();

        // then
        fixture.whenStable().then(() => {
          expectErrorMessage(
            debugElement,
            '#phone-error',
            plainTranslations['main']['userInformation']['phoneNoError']
          );
        });
      })
    );

    it(
      'should display error message for phone input when invalid - long',
      waitForAsync(() => {
        // given

        // when
        fillInput(debugElement, '#phone', '+48123456789123456789');
        fixture.detectChanges();

        // then
        fixture.whenStable().then(() => {
          expectErrorMessage(
            debugElement,
            '#phone-error',
            plainTranslations['main']['userInformation']['phoneNoError']
          );
        });
      })
    );

    it(
      'should display error message for phone input when touched',
      waitForAsync(() => {
        // given

        // when
        fillInput(debugElement, '#phone', null);
        fixture.detectChanges();

        // then
        fixture.whenStable().then(() => {
          expectErrorMessage(
            debugElement,
            '#phone-error',
            plainTranslations['main']['userInformation']['phoneNoError']
          );
        });
      })
    );

    it(
      'should not display error message for birthday input',
      waitForAsync(() => {
        // given
        const date: Date = new Date();
        const birthday: string = new Date(
          date.getFullYear(),
          date.getMonth(),
          date.getDate(),
          12,
          0
        ).toJSON();
        const birthdayInput: string = birthday.substring(
          0,
          birthday.indexOf('T')
        );

        // when
        fillInput(debugElement, '#birthday', birthdayInput);
        fixture.detectChanges();

        // then
        fixture.whenStable().then(() => {
          expect(debugElement.query(By.css('#birthday-error'))).toBeFalsy();
        });
      })
    );

    it(
      'should display error message for birthday input when invalid - too far in the past',
      waitForAsync(() => {
        // given
        const date: Date = new Date();
        const birthday: string = new Date(
          date.getFullYear() - 150,
          date.getMonth(),
          date.getDate(),
          12,
          0
        ).toJSON();
        const birthdayInput: string = birthday.substring(
          0,
          birthday.indexOf('T')
        );

        // when
        fillInput(debugElement, '#birthday', birthdayInput);
        fixture.detectChanges();

        // then
        fixture.whenStable().then(() => {
          expectErrorMessage(
            debugElement,
            '#birthday-error',
            getBirthdayErrorMessage(component)
          );
        });
      })
    );

    it(
      'should display error message for birthday input when invalid - too far in the future',
      waitForAsync(() => {
        // given
        const date: Date = new Date();
        const birthday: string = new Date(
          date.getFullYear(),
          date.getMonth(),
          date.getDate() + 1,
          12,
          0
        ).toJSON();
        const birthdayInput: string = birthday.substring(
          0,
          birthday.indexOf('T')
        );

        // when
        fillInput(debugElement, '#birthday', birthdayInput);
        fixture.detectChanges();

        // then
        fixture.whenStable().then(() => {
          expectErrorMessage(
            debugElement,
            '#birthday-error',
            getBirthdayErrorMessage(component)
          );
        });
      })
    );

    it(
      'should display error message for birthday input when touched',
      waitForAsync(() => {
        // given

        // when
        fillInput(debugElement, '#birthday', null);
        fixture.detectChanges();

        // then
        fixture.whenStable().then(() => {
          expectErrorMessage(
            debugElement,
            '#birthday-error',
            getBirthdayErrorMessage(component)
          );
        });
      })
    );

    it(
      'should not display error message for about input',
      waitForAsync(() => {
        // given

        // when
        fillInput(
          debugElement,
          '#about',
          'Lorem ipsum dolor sit amet, consectetur adipiscing elit. ' +
            'Cras dui ex, auctor ultrices mi vitae, dignissim bibendum lorem.'
        );
        fixture.detectChanges();

        // then
        fixture.whenStable().then(() => {
          expect(debugElement.query(By.css('#about-error'))).toBeFalsy();
        });
      })
    );

    it(
      'should display error message for about input when invalid',
      waitForAsync(() => {
        // given

        // when
        fillInput(
          debugElement,
          '#about',
          'Lorem ipsum dolor sit amet, consectetur adipiscing elit. ' +
            'Cras dui ex, auctor ultrices mi vitae, dignissim bibendum lorem. ' +
            'Praesent ut nisl ipsum. Quisque efficitur luctus mauris, ' +
            'et varius lectus sagittis sit amet. Nam sit amet leo vel nibh ' +
            'rutrum placerat. Duis molestie egestas libero, vulputate placerat ' +
            'nibh tristique non. Aenean tincidunt magna ut ultricies venenatis.'
        );
        fixture.detectChanges();

        // then
        fixture.whenStable().then(() => {
          expectErrorMessage(
            debugElement,
            '#about-error',
            getAboutErrorMessage(component)
          );
        });
      })
    );

    it(
      'should submit button be enabled when form is correctly filled',
      waitForAsync(() => {
        // given
        const date: Date = new Date();
        const birthday: string = new Date(
          date.getFullYear(),
          date.getMonth(),
          date.getDate(),
          12,
          0
        ).toJSON();
        const birthdayInput: string = birthday.substring(
          0,
          birthday.indexOf('T')
        );

        // when
        fillInput(debugElement, '#first-name', 'ValidName');
        fillInput(debugElement, '#last-name', 'ValidName');
        fillInput(debugElement, '#email', 'email@email.com');
        fillInput(debugElement, '#phone', '+48123456789');
        fillInput(debugElement, '#birthday', birthdayInput);
        fixture.detectChanges();

        // then
        fixture.whenStable().then(() => {
          expect(
            debugElement.query(By.css('#submit-button')).nativeElement.disabled
          ).toBeFalse();
        });
      })
    );

    it(
      'should submit button redirect to profile page',
      waitForAsync(() => {
        // given
        const location: Location = TestBed.inject(Location);

        const date: Date = new Date();
        const birthday: string = new Date(
          date.getFullYear(),
          date.getMonth(),
          date.getDate(),
          12,
          0
        ).toJSON();
        const birthdayInput: string = birthday.substring(
          0,
          birthday.indexOf('T')
        );

        // when
        fillInput(debugElement, '#first-name', 'ValidName');
        fillInput(debugElement, '#last-name', 'ValidName');
        fillInput(debugElement, '#email', 'email@email.com');
        fillInput(debugElement, '#phone', '+48123456789');
        fillInput(debugElement, '#birthday', birthdayInput);
        fixture.detectChanges();

        debugElement.query(By.css('#submit-button')).nativeElement.click();

        // then
        fixture.whenStable().then(() => {
          expect(location.path()).toEqual('/user/profile');
        });
      })
    );

    it('should submit button call onFormSubmit', () => {
      // given
      const onFormSubmitSpy: jasmine.Spy = spyOn<any>(
        component,
        'onFormSubmit'
      ).and.callThrough();

      const date: Date = new Date();
      const birthday: string = new Date(
        date.getFullYear(),
        date.getMonth(),
        date.getDate(),
        12,
        0
      ).toJSON();
      const birthdayInput: string = birthday.substring(
        0,
        birthday.indexOf('T')
      );

      // when
      fillInput(debugElement, '#first-name', 'ValidName');
      fillInput(debugElement, '#last-name', 'ValidName');
      fillInput(debugElement, '#email', 'email@email.com');
      fillInput(debugElement, '#phone', '+48123456789');
      fillInput(debugElement, '#birthday', birthdayInput);
      fixture.detectChanges();

      debugElement.query(By.css('#submit-button')).nativeElement.click();

      // then
      expect(onFormSubmitSpy).toHaveBeenCalledTimes(1);
    });

    it('should submit button be disabled when form is empty', () => {
      // given

      // when

      // then
      expect(
        debugElement.query(By.css('#submit-button')).nativeElement.disabled
      ).toBeTrue();
    });

    it(
      'should submit button be disabled when form is wrongly filled',
      waitForAsync(() => {
        // given
        const date: Date = new Date();
        const birthday: string = new Date(
          date.getFullYear(),
          date.getMonth(),
          date.getDate(),
          12,
          0
        ).toJSON();
        const birthdayInput: string = birthday.substring(
          0,
          birthday.indexOf('T')
        );

        // when
        fillInput(debugElement, '#first-name', 'ValidName');
        fillInput(debugElement, '#last-name', 'ValidName');
        fillInput(debugElement, '#email', 'email@');
        fillInput(debugElement, '#phone', '+48123456789');
        fillInput(debugElement, '#birthday', birthdayInput);
        fixture.detectChanges();

        // then
        fixture.whenStable().then(() => {
          expect(
            debugElement.query(By.css('#submit-button')).nativeElement.disabled
          ).toBeTrue();
        });
      })
    );

    it('should reset button be enabled', () => {
      // given

      // when

      // then
      expect(
        debugElement.query(By.css('#reset-button')).nativeElement.disabled
      ).toBeFalse();
    });

    it('should reset button call onFormReset', () => {
      // given
      const resetFormSpy: jasmine.Spy = spyOn<any>(
        component.userInformationFormGroup,
        'reset'
      ).and.callThrough();

      // when
      debugElement.query(By.css('#reset-button')).nativeElement.click();

      // then
      expect(resetFormSpy).toHaveBeenCalledTimes(1);
    });

    it(
      'should reset button clear the form',
      waitForAsync(() => {
        // given
        const date: Date = new Date();
        const birthday: string = new Date(
          date.getFullYear(),
          date.getMonth(),
          date.getDate(),
          12,
          0
        ).toJSON();
        const birthdayInput: string = birthday.substring(
          0,
          birthday.indexOf('T')
        );

        fillInput(debugElement, '#first-name', 'ValidName');
        fillInput(debugElement, '#last-name', 'ValidName');
        fillInput(debugElement, '#email', 'email@email.com');
        fillInput(debugElement, '#phone', '+48123456789');
        fillInput(debugElement, '#birthday', birthdayInput);
        fixture.detectChanges();

        // when
        debugElement.query(By.css('#reset-button')).nativeElement.click();

        // then
        fixture.whenStable().then(() => {
          expect(getInputValue(debugElement, '#first-name')).toBeFalsy();
          expect(getInputValue(debugElement, '#last-name')).toBeFalsy();
          expect(getInputValue(debugElement, '#email')).toBeFalsy();
          expect(getInputValue(debugElement, '#phone')).toBeFalsy();
          expect(getInputValue(debugElement, '#birthday')).toBeFalsy();
        });
      })
    );

    it('should display avatar placeholder', () => {
      // given

      // when
      const avatar: HTMLImageElement | null = debugElement
        .query(By.css('#avatar-preview'))
        .nativeElement.querySelector('#avatar-img');

      // then
      expect(avatar).toBeTruthy();
      expect(avatar?.src).toEqual(
        `${window.location.protocol}//${window.location.host}/assets/avatar-placeholder.png`
      );
    });
  });
});

const expectErrorMessage: Function = (
  debugElement: DebugElement,
  cssSelector: string,
  textContent: string
) => {
  const errorMessage: HTMLSpanElement = debugElement.query(
    By.css(cssSelector)
  ).nativeElement;
  expect(errorMessage).toBeTruthy();
  expect(errorMessage.textContent).toEqual(textContent);
};

@Component({
  selector: `test-component`,
  template: ``,
})
class TestComponent {}

const userDataMock: UserData = {
  firstName: 'firstName',
  lastName: 'lastName',
  email: 'email@email.com',
  phone: '+48123456789',
  birthday: new Date(2000, 1, 1, 1, 0, 0),
  about: 'about',
  avatar: new File([], 'fileName'),
};

const getFirstNameErrorMessage: Function = (
  component: UserInformationFormComponent
): string => {
  let translation: string =
    plainTranslations['main']['userInformation']['firstNameError'];

  translation = translation.replace(
    '{{min}}',
    component.firstNameLengthMin.toString()
  );
  translation = translation.replace(
    '{{max}}',
    component.firstNameLengthMax.toString()
  );

  return translation;
};

const getLastNameErrorMessage: Function = (
  component: UserInformationFormComponent
): string => {
  let translation: string =
    plainTranslations['main']['userInformation']['lastNameError'];

  translation = translation.replace(
    '{{min}}',
    component.firstNameLengthMin.toString()
  );
  translation = translation.replace(
    '{{max}}',
    component.firstNameLengthMax.toString()
  );

  return translation;
};

const getBirthdayErrorMessage: Function = (
  component: UserInformationFormComponent
): string => {
  let translation: string =
    plainTranslations['main']['userInformation']['birthdayError'];

  const datePipe: DatePipe = new DatePipe(TestBed.inject(LOCALE_ID));

  translation = translation.replace(
    '{{min}}',
    datePipe.transform(component.birthdayDateMin, 'd.MM.yyyy') as string
  );
  translation = translation.replace(
    '{{max}}',
    datePipe.transform(component.birthdayDateMax, 'd.MM.yyyy') as string
  );

  return translation;
};

const getAboutErrorMessage: Function = (
  component: UserInformationFormComponent
): string => {
  let translation: string =
    plainTranslations['main']['userInformation']['aboutError'];

  translation = translation.replace(
    '{{max}}',
    component.aboutMaxLength.toString()
  );

  return translation;
};
