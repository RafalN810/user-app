import { Component, OnDestroy, OnInit } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Router } from '@angular/router';
import { Subject, takeUntil } from 'rxjs';
import { UserData } from 'src/app/api/model/user-data';
import { DateValidator } from 'src/app/shared/validators/date/date-validator';
import { FileValidator } from 'src/app/shared/validators/file/file-validator';
import { UserInformationWrapperService } from '../services/user-information-wrapper.service';

/**
 * Component responsible for gathering user information.
 */
@Component({
  selector: 'app-user-information-form',
  templateUrl: './user-information-form.component.html',
  styleUrls: ['./user-information-form.component.scss'],
  providers: [UserInformationWrapperService],
})
export class UserInformationFormComponent implements OnInit, OnDestroy {
  private readonly destroy$: Subject<void> = new Subject();
  private readonly defaultImgSource: string = '/assets/avatar-placeholder.png';
  public userInformationFormGroup: FormGroup;
  public imgSource: string = this.defaultImgSource;

  public readonly aboutMaxLength: number = 255;
  public readonly avatarFileSizeInB = 1048576;
  public readonly avatarFormat = ['.png', '.jpg', '.jpeg'];
  public readonly avatarFormatJoin = this.avatarFormat.join(' ,');
  public readonly birthdayDateMin: Date = new Date(1900, 0, 1);
  public readonly birthdayDateMax: Date = new Date();
  public readonly firstNameRegExpPattern: RegExp =
    /^([A-Za-z]*)$|^([A-Za-z]{3,}\s[A-Za-z]{3,})$/;
  public readonly lastNameRegExpPattern: RegExp =
    /^([A-Za-z]*)$|^([A-Za-z]{3,}(\s|\-)[A-Za-z]{3,})$/;
  public readonly firstNameLengthMin: number = 2;
  public readonly firstNameLengthMax: number = 20;
  public readonly lastNameLengthMin: number = 2;
  public readonly lastNameLengthMax: number = 20;
  public readonly phoneRegExpPattern: RegExp =
    /(^[\+]{1}[0-9]{9,11}$)|(^[0-9]{7,9}$)/;

  get submitButtonDisabled(): boolean {
    return this.userInformationFormGroup.invalid;
  }

  get firstNameControl(): AbstractControl {
    return this.userInformationFormGroup.get('firstName') as AbstractControl;
  }

  get lastNameControl(): AbstractControl {
    return this.userInformationFormGroup.get('lastName') as AbstractControl;
  }

  get emailControl(): AbstractControl {
    return this.userInformationFormGroup.get('email') as AbstractControl;
  }

  get phoneControl(): AbstractControl {
    return this.userInformationFormGroup.get('phone') as AbstractControl;
  }

  get birthdayControl(): AbstractControl {
    return this.userInformationFormGroup.get('birthday') as AbstractControl;
  }

  get avatarControl(): AbstractControl {
    return this.userInformationFormGroup.get('avatar') as AbstractControl;
  }

  get aboutControl(): AbstractControl {
    return this.userInformationFormGroup.get('about') as AbstractControl;
  }

  constructor(
    private readonly userInformationWrapperService: UserInformationWrapperService,
    private readonly formBuilder: FormBuilder,
    private readonly router: Router
  ) {}

  public ngOnInit(): void {
    this.initUserInformationFormGroup();
    this.listenToAvatarChosen();
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.unsubscribe();
  }

  /**
   * Handles form submit action.
   */
  public onFormSubmit(): void {
    this.userInformationWrapperService
      .registerUser(this.mapToUserData(this.userInformationFormGroup.value))
      .pipe(takeUntil(this.destroy$))
      .subscribe((data) => this.handleFormSubmitted(data));
  }

  /**
   * Resets the form.
   */
  public onFormReset(): void {
    this.userInformationFormGroup.reset();
  }

  private initUserInformationFormGroup(): void {
    this.userInformationFormGroup = this.formBuilder.group({
      firstName: [
        null,
        [
          Validators.required,
          Validators.minLength(this.firstNameLengthMin),
          Validators.maxLength(this.firstNameLengthMax),
          Validators.pattern(this.firstNameRegExpPattern),
        ],
      ],
      lastName: [
        null,
        [
          Validators.required,
          Validators.minLength(this.lastNameLengthMin),
          Validators.maxLength(this.lastNameLengthMax),
          Validators.pattern(this.lastNameRegExpPattern),
        ],
      ],
      email: [null, [Validators.required, Validators.email]],
      phone: [
        null,
        [Validators.required, Validators.pattern(this.phoneRegExpPattern)],
      ],
      birthday: [
        null,
        [
          Validators.required,
          DateValidator.validateDateMin(this.birthdayDateMin),
          DateValidator.validateDateMax(this.birthdayDateMax),
        ],
      ],
      about: [null, Validators.maxLength(this.aboutMaxLength)],
      avatar: [
        null,
        [
          FileValidator.validateFileSize(this.avatarFileSizeInB),
          FileValidator.validateFileFormat(this.avatarFormat),
        ],
      ],
    });
  }

  private listenToAvatarChosen(): void {
    this.avatarControl?.valueChanges
      .pipe(takeUntil(this.destroy$))
      .subscribe((value: File) => this.handleAvatarChosen(value));
  }

  private handleAvatarChosen(file: File): void {
    if (this.avatarControl.valid && file) {
      const reader: FileReader = new FileReader();

      reader.onload = (_) => {
        this.imgSource = (reader.result as string) ?? this.defaultImgSource;
      };
      reader.readAsDataURL(file);
      return;
    }
    this.imgSource = this.defaultImgSource;
  }

  private handleFormSubmitted(data: UserData): void {
    console.log(data);
    this.router.navigate(['user/profile']);
  }

  private mapToUserData(formValues: { [key: string]: any }): UserData {
    return {
      firstName: formValues['firstName'],
      lastName: formValues['lastName'],
      email: formValues['email'],
      phone: formValues['phone'],
      birthday: formValues['birthday'],
      about: formValues['about'],
      avatar: formValues['avatar'],
    };
  }
}
