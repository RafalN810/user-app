import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { UserInformationFormComponent } from './user/user-information-form/user-information-form.component';
import { UserProfileComponent } from './user/user-profile/user-profile.component';

/**
 * Module responsible for defining app functionalities.
 */
@NgModule({
  declarations: [UserInformationFormComponent, UserProfileComponent],
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  exports: [UserInformationFormComponent, UserProfileComponent],
})
export class MainModule {}
