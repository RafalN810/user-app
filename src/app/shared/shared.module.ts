import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FileUploadInputComponent } from './file-upload-input/file-upload-input.component';
import { ClickOutsideDirective } from './directives/click-outside.directive';
import { HttpClient } from '@angular/common/http';
import {
  TranslateLoader,
  TranslateModule,
  TranslateService,
} from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

export const httpLoaderFactory = (http: HttpClient): TranslateLoader =>
  new TranslateHttpLoader(http, './assets/i18n/', '.json');

/**
 * Module responsible for defining all shared functionalities.
 */
@NgModule({
  declarations: [
    FileUploadInputComponent,
    // directives
    ClickOutsideDirective,
  ],
  imports: [
    CommonModule,
    TranslateModule.forRoot({
      defaultLanguage: 'en',
      loader: {
        provide: TranslateLoader,
        useFactory: httpLoaderFactory,
        deps: [HttpClient],
      },
    }),
  ],
  exports: [
    FileUploadInputComponent,
    // directives
    ClickOutsideDirective,
    // modules
    TranslateModule,
  ],
})
export class SharedModule {
  // super basic translation configuration
  constructor(private readonly translateService: TranslateService) {
    this.translateService.addLangs(['en', 'pl']);
    const browserLang: string | undefined =
      this.translateService.getBrowserLang();

    if (browserLang && this.translateService.getLangs().includes(browserLang)) {
      this.translateService.use(browserLang);
    } else {
      this.translateService.use('en');
    }
  }
}
