import { Component, DebugElement, ViewChild } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { FileUploadInputComponent } from './file-upload-input.component';

describe('FileUploadInputComponent', () => {
  let hostComponent: TestHostComponent;
  let fixture: ComponentFixture<TestHostComponent>;
  let component: FileUploadInputComponent;
  let debugElement: DebugElement;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [FileUploadInputComponent, TestHostComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TestHostComponent);
    hostComponent = fixture.componentInstance;
    fixture.detectChanges();
    component = hostComponent.fileUploadComponent;
    fixture.detectChanges();
    debugElement = fixture.debugElement;
  });

  describe('class', () => {
    it('should create the component', () => {
      // given

      // when

      // then
      expect(component).toBeTruthy();
    });

    it('should correctly bind onChange method', () => {
      // given
      const onChangeFn: Function = () => console.log('test');

      // when
      component.registerOnChange(onChangeFn);

      // then
      expect(component.onChange).toBe(onChangeFn);
    });

    it('should correctly bind onTouched method', () => {
      // given
      const onTouchedFn: Function = () => console.log('test');

      // when
      component.registerOnTouched(onTouchedFn);

      // then
      expect(component.onTouched).toBe(onTouchedFn);
    });
  });

  describe('DOM', () => {
    it('should map component inputs', () => {
      // given

      // when
      const input: HTMLInputElement = debugElement.query(
        By.css('input')
      ).nativeElement;

      // then
      expect(input.accept).toEqual(hostComponent.accept);
      expect(input.id).toEqual(hostComponent.id);
      expect(input.name).toEqual(hostComponent.name);
    });
  });
});

@Component({
  selector: `host-component`,
  template: `<app-file-upload-input
    [accept]="accept"
    [id]="id"
    [name]="name"
  ></app-file-upload-input>`,
})
class TestHostComponent {
  @ViewChild(FileUploadInputComponent)
  public fileUploadComponent: FileUploadInputComponent;
  public accept: string = '.jpg, .png';
  public id: string = 'id';
  public name: string = 'name';
}
