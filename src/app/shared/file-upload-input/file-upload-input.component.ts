import {
  Component,
  ElementRef,
  forwardRef,
  Input,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

/**
 * Component responsible for file upload.
 */
@Component({
  selector: 'app-file-upload-input',
  templateUrl: './file-upload-input.component.html',
  styleUrls: ['./file-upload-input.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => FileUploadInputComponent),
      multi: true,
    },
  ],
  encapsulation: ViewEncapsulation.None,
})
export class FileUploadInputComponent implements ControlValueAccessor {
  @ViewChild('fileInput') public input: ElementRef;
  @Input() public accept: string;
  @Input() public id: string;
  @Input() public name: string;

  private file: File | null;

  public onChange: Function = (value: File) => {};
  public onTouched: Function = () => {};

  get value(): File | null {
    return this.file;
  }

  set value(value: File | null) {
    if (value != this.file) {
      this.file = value;
      this.onChange.call(this);
    }
  }

  constructor() {}

  public writeValue(value: File): void {
    if (!value && this.input) {
      this.input.nativeElement.value = '';
    }
    this.file = value;
  }

  public registerOnChange(fn: Function): void {
    this.onChange = fn;
  }

  public registerOnTouched(fn: Function): void {
    this.onTouched = fn;
  }

  /**
   * Handles file change action.
   * @param event the triggered file change action event
   */
  public onFileChosen(event: Event): void {
    const eventTarget: { [key: string]: any } = event.target as {
      [key: string]: any;
    };
    [this.file] = eventTarget['files'];
    this.onTouched.call(this);
    this.onChange.call(this, this.file);
  }
}
