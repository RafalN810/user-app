import { AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';

/**
 * Class responsible for validating dates.
 */
export class DateValidator {
  /**
   * Returns validator function for date min.
   * @param date the min date allowed
   * @returns validator function for AbstractControl
   */
  public static validateDateMin(date: Date): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      return new Date(control.value) < date
        ? { dateMin: { value: control.value } }
        : null;
    };
  }

  /**
   * Returns validator function for date max.
   * @param date the max date allowed
   * @returns validator function for AbstractControl
   */
  public static validateDateMax(date: Date): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      return new Date(control.value) > date
        ? { dateMax: { value: control.value } }
        : null;
    };
  }
}
