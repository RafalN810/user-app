import { FormControl, ValidatorFn } from '@angular/forms';
import { DateValidator } from './date-validator';

describe('DateValidator', () => {
  it('should return null when validating date min - date equal', () => {
    // given
    const dateMin: Date = new Date(2000, 1, 1, 12, 0, 0, 0);
    const validatorFn: ValidatorFn = DateValidator.validateDateMin(dateMin);
    const control: FormControl = new FormControl(
      new Date(2000, 1, 1, 12, 0, 0, 0)
    );

    // when
    const result: { [key: string]: unknown } | null = validatorFn.call(
      this,
      control
    );

    // then
    expect(result).toBeNull();
  });

  it('should return null when validating date min - date valid', () => {
    // given
    const dateMin: Date = new Date(2000, 1, 1, 12, 0, 0, 0);
    const validatorFn: ValidatorFn = DateValidator.validateDateMin(dateMin);
    const control: FormControl = new FormControl(
      new Date(2001, 1, 1, 12, 0, 0, 0)
    );

    // when
    const result: { [key: string]: unknown } | null = validatorFn.call(
      this,
      control
    );

    // then
    expect(result).toBeNull();
  });

  it('should return validation errors when validating date min - date invalid', () => {
    // given
    const dateMin: Date = new Date(2000, 1, 1, 12, 0, 0, 0);
    const validatorFn: ValidatorFn = DateValidator.validateDateMin(dateMin);
    const control: FormControl = new FormControl(
      new Date(1999, 1, 1, 12, 0, 0, 0)
    );

    // when
    const result: { [key: string]: unknown } | null = validatorFn.call(
      this,
      control
    );

    // then
    expect(result).toBeTruthy();
    expect(Object.keys(result as { [key: string]: unknown })).toContain(
      'dateMin'
    );
  });

  it('should return null when validating date max - date equal', () => {
    // given
    const dateMax: Date = new Date(2000, 1, 1, 12, 0, 0, 0);
    const validatorFn: ValidatorFn = DateValidator.validateDateMax(dateMax);
    const control: FormControl = new FormControl(
      new Date(2000, 1, 1, 12, 0, 0, 0)
    );

    // when
    const result: { [key: string]: unknown } | null = validatorFn.call(
      this,
      control
    );

    // then
    expect(result).toBeNull();
  });

  it('should return null when validating date max - date valid', () => {
    // given
    const dateMax: Date = new Date(2000, 1, 1, 12, 0, 0, 0);
    const validatorFn: ValidatorFn = DateValidator.validateDateMax(dateMax);
    const control: FormControl = new FormControl(
      new Date(1999, 1, 1, 12, 0, 0, 0)
    );

    // when
    const result: { [key: string]: unknown } | null = validatorFn.call(
      this,
      control
    );

    // then
    expect(result).toBeNull();
  });

  it('should return validation errors when validating date max - date invalid', () => {
    // given
    const dateMax: Date = new Date(2000, 1, 1, 12, 0, 0, 0);
    const validatorFn: ValidatorFn = DateValidator.validateDateMax(dateMax);
    const control: FormControl = new FormControl(
      new Date(2001, 1, 1, 12, 0, 0, 0)
    );

    // when
    const result: { [key: string]: unknown } | null = validatorFn.call(
      this,
      control
    );

    // then
    expect(result).toBeTruthy();
    expect(Object.keys(result as { [key: string]: unknown })).toContain(
      'dateMax'
    );
  });
});
