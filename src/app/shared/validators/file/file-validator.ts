import { AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';

/**
 * Class responsible for validating dates.
 */
export class FileValidator {
  /**
   * Returns validator function for file size.
   * @param fileSize the max file size allowed
   * @returns validator function for AbstractControl
   */
  public static validateFileSize(fileSize: number): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      return (control.value as File)?.size > fileSize
        ? { fileSize: { value: control.value } }
        : null;
    };
  }

  /**
   * Returns validator function for file size.
   * @param formats the allowed file formats
   * @returns validator function for AbstractControl
   */
  public static validateFileFormat(formats: string[]): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      if (control.value) {
        const name: string = (control.value as File).name;
        const type: string = name?.substring(name.lastIndexOf('.'));

        const upperCaseFormats: string[] = formats.map((format) =>
          format.toUpperCase()
        );

        return upperCaseFormats.indexOf(type.toUpperCase()) < 0
          ? { fileFormat: { value: control.value } }
          : null;
      }
      return null;
    };
  }
}
