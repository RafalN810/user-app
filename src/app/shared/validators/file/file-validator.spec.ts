import { FormControl, ValidatorFn } from '@angular/forms';
import { FileValidator } from './file-validator';

describe('FileValidator', () => {
  it('should return null when validating file size - size equal', () => {
    // given
    const fileSize: number = 0;
    const validatorFn: ValidatorFn = FileValidator.validateFileSize(fileSize);
    const control: FormControl = new FormControl(new File([], 'fileName.jpg'));

    // when
    const result: { [key: string]: unknown } | null = validatorFn.call(
      this,
      control
    );

    // then
    expect(result).toBeNull();
  });

  it('should return null when validating file size - file smaller', () => {
    // given
    const fileSize: number = 100;
    const validatorFn: ValidatorFn = FileValidator.validateFileSize(fileSize);
    const control: FormControl = new FormControl(new File([], 'fileName.jpg'));

    // when
    const result: { [key: string]: unknown } | null = validatorFn.call(
      this,
      control
    );

    // then
    expect(result).toBeNull();
  });

  it('should return validation errors when validating file size', () => {
    // given
    // workaround to trick the validator, we don't check the negative values
    const fileSize: number = -1;
    const validatorFn: ValidatorFn = FileValidator.validateFileSize(fileSize);
    const control: FormControl = new FormControl(new File([], 'fileName.jpg'));

    // when
    const result: { [key: string]: unknown } | null = validatorFn.call(
      this,
      control
    );

    // then
    expect(result).toBeTruthy();
    expect(Object.keys(result as { [key: string]: unknown })).toContain(
      'fileSize'
    );
  });

  it('should return null when validating file format', () => {
    // given
    const fileFormats: string[] = ['.jpg', '.png'];
    const validatorFn: ValidatorFn =
      FileValidator.validateFileFormat(fileFormats);
    const control: FormControl = new FormControl(new File([], 'fileName.jpg'));

    // when
    const result: { [key: string]: unknown } | null = validatorFn.call(
      this,
      control
    );

    // then
    expect(result).toBeNull();
  });

  it('should return validation errors when validating file format', () => {
    // given
    const fileFormats: string[] = ['.jpg', '.png'];
    const validatorFn: ValidatorFn =
      FileValidator.validateFileFormat(fileFormats);
    const control: FormControl = new FormControl(new File([], 'fileName.pdf'));

    // when
    const result: { [key: string]: unknown } | null = validatorFn.call(
      this,
      control
    );

    // then
    expect(result).toBeTruthy();
    expect(Object.keys(result as { [key: string]: unknown })).toContain(
      'fileFormat'
    );
  });
});
