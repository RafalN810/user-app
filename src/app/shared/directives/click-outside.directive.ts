import {
  Directive,
  ElementRef,
  EventEmitter,
  HostListener,
  Output,
} from '@angular/core';

/**
 * Directive responsible for catching click events outside of the HTML element.
 */
@Directive({
  selector: '[appClickOutside]',
})
export class ClickOutsideDirective {
  @Output('appClickOutside') public readonly clickOutside: EventEmitter<void> =
    new EventEmitter<void>();

  constructor(private elementRef: ElementRef) {}

  /**
   * Handles document click event, emitting a value whenever the user clicks outside of the HTML element.
   * @param event the click event
   */
  @HostListener('document:click', ['$event'])
  public onClick(event: Event) {
    if (!this.elementRef.nativeElement.contains(event.target)) {
      this.clickOutside.emit();
    }
  }
}
