import { Component, DebugElement } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { ClickOutsideDirective } from './click-outside.directive';

describe('ClickOutsideDirective', () => {
  let component: TestComponent;
  let fixture: ComponentFixture<TestComponent>;

  beforeEach(async () => {
    fixture = await TestBed.configureTestingModule({
      declarations: [ClickOutsideDirective, TestComponent],
    }).createComponent(TestComponent);
  });

  let nonDirectiveElement: DebugElement;
  let directiveElement: DebugElement;

  beforeEach(() => {
    component = fixture.componentInstance;
    fixture.detectChanges();

    nonDirectiveElement = fixture.debugElement.query(By.css('#without'));

    directiveElement = fixture.debugElement.query(
      By.directive(ClickOutsideDirective)
    );
  });

  it('should trigger handling method when clicked outside', () => {
    // given
    const handlingMethodSpy: jasmine.Spy = spyOn<TestComponent>(
      component,
      'onClickOutside'
    ).and.callThrough();

    // when
    nonDirectiveElement.nativeElement.click();

    // then
    expect(handlingMethodSpy).toHaveBeenCalledTimes(1);
  });

  it('should not trigger handling method when clicked inside', () => {
    // given
    const handlingMethodSpy: jasmine.Spy = spyOn<TestComponent>(
      component,
      'onClickOutside'
    ).and.callThrough();

    // when
    directiveElement.nativeElement.click();

    // then
    expect(handlingMethodSpy).not.toHaveBeenCalled();
  });
});

@Component({
  template: `<div id="with" (appClickOutside)="onClickOutside()"></div>
    <div id="without"></div>`,
})
class TestComponent {
  public onClickOutside(): void {}
}
