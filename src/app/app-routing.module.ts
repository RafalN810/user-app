import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NotFoundComponent } from './core/components/not-found/not-found.component';
import { UserInformationFormComponent } from './main/user/user-information-form/user-information-form.component';
import { UserProfileComponent } from './main/user/user-profile/user-profile.component';

const routes: Routes = [
  {
    path: 'user',
    children: [
      { path: 'form', component: UserInformationFormComponent },
      { path: 'profile', component: UserProfileComponent },
    ],
  },
  {
    path: '**',
    component: NotFoundComponent,
  },
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
